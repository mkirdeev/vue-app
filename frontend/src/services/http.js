import axios from 'axios';
import { baseURL } from '@/constants/index';


const instance = axios.create({
  baseURL,
  timeout: 3000,
  headers: { 'X-Custom-Header': 'foobar' },
});

export default instance;
