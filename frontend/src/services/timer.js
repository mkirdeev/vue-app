
export default {
  waitFor(seconds) {
    return new Promise((resolve) => {
      window.setTimeout(resolve, seconds * 1000);
    });
  },
};
