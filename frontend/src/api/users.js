
import http from '@/services/http';


export default {
  getUsers() {
    return http.get('/users');
  },
  freeApples() {
    return http.get('/apples/free');
  },
  grabApple(userId) {
    return http.get(`/users/${userId}/grab`);
  },
};
