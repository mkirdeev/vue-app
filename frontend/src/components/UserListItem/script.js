import { mapGetters, mapActions } from 'vuex';

export default {
  name: 'UselListItem',
  props: {
    user: Object,
  },
  computed: {
    ...mapGetters([
      'isEmptyBasket',
    ]),
  },
  methods: {
    ...mapActions([
      'grabApple',
    ]),
    grab() {
      this.grabApple(this.user.id, this.getLastBasketItem);
    },
  },
};
