import { mapActions } from 'vuex';
import timer from '@/services/timer';

export default {
  name: 'AppAlert',
  props: {
    data: Object,
  },
  methods: {
    ...mapActions([
      'clearError',
    ]),
  },
  mounted() {
    timer.waitFor(10).then(() => {
      this.clearError(this.data.id);
    });
  },
};
