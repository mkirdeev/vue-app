import { mapGetters } from 'vuex';

export default {
  name: 'Basket',
  computed: {
    ...mapGetters([
      'basket',
    ]),
  },
};
