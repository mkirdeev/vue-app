import Vuex from 'vuex';
import Vue from 'vue';
import users from './users';
import basket from './basket';
import base from './base';


Vue.use(Vuex);


export default new Vuex.Store({
  modules: {
    users,
    basket,
    base,
  },
});
