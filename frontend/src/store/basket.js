import home from '@/api/home';
import { GRAB_APPLE, RECEIVE_BASKET } from '@/mutation-types';

const initialState = {
  entities: [],
};

const actions = {
  getBasket({ commit }) {
    home.getBasket().then(({ data }) => {
      commit(RECEIVE_BASKET, data);
    });
  },
};

const mutations = {
  [RECEIVE_BASKET]: (state, basket) => {
    state.entities = basket;
  },
  [GRAB_APPLE]: (state) => {
    state.entities.splice(state.entities.length - 1, 1);
  },
};

const getters = {
  basket(state) {
    return state.entities;
  },
  isEmptyBasket(state) {
    return state.entities.length === 0;
  },
};

export default {
  state: initialState,
  actions,
  mutations,
  getters,
};
