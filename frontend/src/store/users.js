import usersApi from '@/api/users';
import keyBy from 'lodash-es/keyBy';
import values from 'lodash-es/values';
import mapValues from 'lodash-es/mapValues';

import { GRAB_APPLE, FREE_APPLES, RECEIVE_USERS, SHOW_ERROR } from '@/mutation-types';

const initialState = {
  entities: [],
};

const actions = {
  getUsers({ commit }) {
    usersApi.getUsers().then(({ data }) => {
      commit(RECEIVE_USERS, data);
    });
  },
  freeApples({ commit }) {
    usersApi.freeApples().then(() => {
      commit(FREE_APPLES);
    });
  },
  grabApple({ commit }, userId, apple) {
    usersApi.grabApple(userId).then(({ data }) => {
      const { success, error } = data;
      if (success) {
        commit({
          type: GRAB_APPLE,
          userId,
          apple,
        });
      } else {
        commit({
          type: SHOW_ERROR,
          error,
        });
      }
    });
  },
};

const mutations = {
  [RECEIVE_USERS]: (state, users) => {
    state.entities = keyBy(users, 'id');
  },
  [FREE_APPLES]: (state) => {
    state.entities = mapValues(state.entities, entity => ({
      ...entity,
      apples: [],
    }));
  },
  [GRAB_APPLE]: (state, { userId, apple }) => {
    const user = state.entities[userId];
    state.entities = {
      ...state.entities,
      [userId]: {
        ...user,
        apples: [
          ...user.apples,
          apple,
        ],
      },
    };
  },
};

const getters = {
  users(state) {
    return values(state.entities);
  },
  usersIds(state) {
    return state.allIds;
  },
  getUserData: ({ byId, applesCountById }) => (userId) => {
    const { name } = byId[userId];
    return {
      name,
      count: applesCountById[userId] || 0,
    };
  },
};

export default {
  state: initialState,
  actions,
  mutations,
  getters,
};
