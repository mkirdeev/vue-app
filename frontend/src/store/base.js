import reject from 'lodash-es/reject';
import uniqueId from 'lodash-es/uniqueId';
import { SHOW_ERROR, CLEAR_ERROR } from '@/mutation-types';

const initialState = {
  errors: [],
};

const actions = {
  clearError({ commit }, errorId) {
    commit(CLEAR_ERROR, { errorId });
  },
};

const mutations = {
  [SHOW_ERROR]: (state, { error }) => {
    state.errors.push({
      id: uniqueId(),
      error,
    });
  },
  [CLEAR_ERROR]: (state, { errorId }) => {
    state.errors = reject(state.errors, { id: errorId });
  },
};

const getters = {
  getErrors(state) {
    return state.errors;
  },
};

export default {
  state: initialState,
  actions,
  mutations,
  getters,
};
