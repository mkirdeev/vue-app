/* eslint-disable import/prefer-default-export */

export const GRAB_APPLE = 'grab_apple';
export const RECEIVE_BASKET = 'receive_basket';
export const FREE_APPLES = 'free_apples';
export const RECEIVE_USERS = 'receive_users';
export const SHOW_ERROR = 'show_error';
export const CLEAR_ERROR = 'clear_error';
