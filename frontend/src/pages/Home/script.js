import { mapActions, mapGetters } from 'vuex';
import Basket from '@/components/Basket/Basket';
import UserListItem from '@/components/UserListItem/UserListItem';

export default {
  mounted() {
    this.getUsers();
    this.getBasket();
  },
  components: {
    Basket,
    UserListItem,
  },
  computed: {
    ...mapGetters([
      'users',
      'grabErrors',
    ]),
  },
  methods: {
    ...mapActions([
      'getUsers',
      'getBasket',
      'freeApples',
    ]),
    async freeApplesHandler() {
      await this.freeApples();
      this.getBasket();
    },
  },
};
