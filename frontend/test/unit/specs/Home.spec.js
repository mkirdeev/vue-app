import Vuex from 'vuex';
import BootstrapVue from 'bootstrap-vue';
import { shallow, createLocalVue } from '@vue/test-utils';
import Home from '@/pages/Home/Home';

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(BootstrapVue);

describe('Home.vue', () => {
  let getters;
  let store;
  let actions;

  beforeEach(() => {
    getters = {
      users: () => [],
    };
    actions = {
      getUsers: () => [],
      getBasket: () => [],
    };

    store = new Vuex.Store({
      getters,
      actions,
    });
  });

  it('should render correct contents', () => {
    const wrapper = shallow(Home, { store, localVue });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
